<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package daidoko
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer container-fluid text-center border-top pt-4 mt-5 text-white">
		<div class="site-info row">
			<div class="col">
				<p style="font-size: 12px;">&copy;株式会社リーブル</p>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
