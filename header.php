<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package daidoko
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script>
		(function(d) {
			var config = {
					kitId: 'ldf2fdi',
					scriptTimeout: 3000,
					async: true
				},
				h = d.documentElement,
				t = setTimeout(function() {
					h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
				}, config.scriptTimeout),
				tk = d.createElement("script"),
				f = false,
				s = d.getElementsByTagName("script")[0],
				a;
			h.className += " wf-loading";
			tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
			tk.async = true;
			tk.onload = tk.onreadystatechange = function() {
				a = this.readyState;
				if (f || a && a != "complete" && a != "loaded") return;
				f = true;
				clearTimeout(t);
				try {
					Typekit.load(config)
				} catch (e) {}
			};
			s.parentNode.insertBefore(tk, s)
		})(document);
	</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'daidoko'); ?></a>

		<header id="masthead" class="site-header">
			<div class="site-branding">
				<?php
				the_custom_logo();
						if (is_front_page() && is_home()) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
				<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></p>
				<?php
				endif;
						$daidoko_description = get_bloginfo('description', 'display');
						if ($daidoko_description || is_customize_preview()) :
					?>
					<p class="site-description"><?php echo $daidoko_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e('Primary Menu', 'daidoko'); ?></button>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->

		<!-- Top画像 -->
		<?php if (is_home() || is_front_page()) : ?>
			<div class="container pb-2 mt-3" style="margin-bottom:-3rem;">
				<div class="row">
					<div class="col-8 col-sm mx-auto text-center top-meat">
						<img src="<?php header_image(); ?>" height="auto" width="400" alt="" />
					</div>
				</div>
			</div><!-- #Top画像 -->
		<?php endif; ?>

		<!-- 2トップ店名ロゴ -->
		<div class="container mt-5">
			<div class="d-flex justify-content-center no-gutters align-items-end">
				<?php if (is_daidoko_or_hanare('daidoko')) : ?>
					<div class="text-center header-daidoko-logo">
						<?php
					$category_id   = get_cat_ID('だいどこ');
					$category_link = get_category_link($category_id);
							?>
						<a href="<?php echo esc_url($category_link); ?>">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/daidoko_headlogo_h_2.svg" alt="">
						</a>
					</div>
				<?php endif; ?>
				<?php if (is_daidoko_or_hanare('hanare')) : ?>
					<div class="text-center">
						<?php
					$category_id   = get_cat_ID('HANARE');
					$category_link = get_category_link($category_id);
							?>
						<a href="<?php echo esc_url($category_link); ?>">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/hanare_headlogo_h.svg" alt="">
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div><!-- #2トップ店名ロゴ -->

		<div id="content" class="site-content text-white">


			<!-- Top画像 -->
			<div class="container my-5">
				<div class="row">
					<div class="col mx-auto text-center">
						<?php
						if (!is_home() && !is_front_page() && (is_singular() || is_archive())) :
							$cat_slug = "";
							if (is_singular()) {
								$cats = get_the_category();
								foreach ($cats as $cat) :
									if ($cat->parent) $cat_slug = $cat->slug;
								endforeach;
							}
							if (is_archive()) {
								$term = get_queried_object();
								$cat_slug = $term->slug;
							}
							if ('hanare' == $cat_slug) {
								$daidoko_src_url = get_theme_mod('daidoko_header_images_setting_hanare',  "") ?: get_stylesheet_directory_uri() . "/img/top-page-beaf-hanare.png";
								$img_class = "hanare-top-image";
							} else {
								$daidoko_src_url = get_theme_mod('daidoko_header_images_setting_main',  "") ?: get_stylesheet_directory_uri() . "/img/top-page-beaf.png";
								$img_class = "daidoko-top-image";
							}
							?>
							<img src="<?php echo esc_url($daidoko_src_url); ?>" class="<?php echo esc_attr($img_class); ?>" alt="">
						<?php elseif (!is_home() && !is_front_page()) : //それ以外 
							?>
							<img src="<?php header_image(); ?>" height="auto" width="400" alt="" />
						<?php endif; ?>
					</div>
				</div>
			</div><!-- #Top画像 -->