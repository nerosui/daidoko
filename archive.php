<?php
/**
 *
 * Template Name: archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package daidoko
 */

get_header();
?>

	<!-- title -->
	<div class="container mt-5">
		<div class="row">
			<div class="col mx-auto p-0">
				<h1 class="text-center mb-3 ">NEWS</h1>
			</div>
		</div>
	</div><!-- #title -->

	<!-- main -->
	<main id="main" class="site-main container mt-3">
		<div class="row m-0">
			<div class="col mx-auto my-bg-daidoko text-black">
				<div id="container" <?php post_class(); ?>>
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) :
							the_post();
							?>
					<!-- loop section -->
					<section class="row my-archive-section">

						<!-- img -->
						<div class="col-4 pl-0 pr-4">
							<a href="<?php the_permalink(); ?>">
							<?php
							if ( has_post_thumbnail() ) { // 投稿にアイキャッチ画像が割り当てられているかチェックします。
								the_post_thumbnail();
							} else {
								echo '<img class="img-fluid" src="', get_stylesheet_directory_uri() ,'/img/noimage.png" alt="noimage"></a>';
							}
							?>
							</a>
						</div><!-- #img -->

						<!-- category/date/taitle -->
						<div class="col-8 pl-4 pr-0">

							<!-- category -->
							<div class="mb-4 my-category">	
							<?php
								$category = get_the_category();
							if ( $category ) {
								foreach ( $category as $cat ) {
									if ( $cat->category_nicename == 'hanare' || $cat->category_nicename == 'daidoko' ) {
										echo '<a href="' . get_category_link( $cat->cat_ID ) . '"><img class="pr-3 ', $cat->category_nicename, '" src="', get_stylesheet_directory_uri() ,'/img/', $cat->category_nicename, '_logo_mini.svg" alt="', $cat->cat_name, '"></a>';
									}
								}
							}
							?>
							</div>	<!-- #category -->

							<!-- date -->
							<time class="my-date font-weight-demibold"><?php the_time( '(Y年n月j日)' ); ?>
							</time>	<!-- #date -->

							<!-- title -->
							<h2 class="my-archive-h2 m-0">
								<a href="<?php the_permalink(); ?>">
								<?php echo the_title(); ?>
								</a>
							</h2>	<!-- #taitle -->

						</div>	<!-- #category/date/taitle -->
					</section>	<!-- #loop section -->
							<?php
					endwhile;
endif;
					?>
				</div>
			</div>
		</div>
	</main><!-- #main -->

	<!-- backbutton -->
	<div class="container my-5">
		<div class="row">
			<div class="col mx-auto mb-5 text-center">
				<a href="<?php echo home_url( '/' ); ?>">
					<button type="button" class="my-backbutton">戻る</button>
				</a>
			</div>
		</div>
	</div><!-- #backbutton -->
	
<?php
get_footer();

