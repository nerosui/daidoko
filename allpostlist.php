<?php
/**
 * Template Name: all post list
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package daidoko
 */

get_header();
?>

	<!-- Top画像 -->
	<div class="container">
		<div class="row">
			<div class="col mx-auto text-center">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/archive-top.png" alt="">
			</div>
		</div>
	</div><!-- #Top画像 -->

	<!-- title -->
	<div class="container mt-5">
		<div class="row">
			<div class="col mx-auto p-0">
				<h1 class="text-center mb-3 ">NEWS</h1>
			</div>
		</div>
	</div><!-- #title -->

	<!-- main -->
	<main id="main" class="site-main container mt-3">
		<div class="row m-0">
			<div class="col mx-auto my-bg-daidoko text-black">
			
        <div id="container" <?php post_class();?>>

          <?php
          $paged = (int) get_query_var('paged');
          $args = array(
            'paged' => $paged,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish'
          );
          $the_query = new WP_Query($args);

          if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();
          ?>
					<!-- loop section -->
					<section class="row my-archive-section">

						<!-- img -->
						<div class="col-4 pl-0 pr-4">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail(); ?>
							</a>
						</div>

						<!-- category/date/taitle -->
						<div class="col-8 pl-4 pr-0">

							<!-- category -->
							<div class="mb-3">	
								<?php
								$categories = get_the_category();
								foreach($categories as $cat) {
								echo '<img class="pr-3" src="', bloginfo('stylesheet_directory') ,'/img/', $cat->slug, '_logo_mini.svg" alt="', $cat->slug,'">
								</a>';
								}
							?>
							</div>	<!-- #category -->

							<!-- date -->
							<time class="my-date font-weight-demibold"><?php the_time('(Y年n月j日)'); ?>
							</time>	<!-- #date -->

							<!-- title -->
							<h2 class="my-archive-h2 m-0"><?php echo the_title();?>
							</h2>	<!-- #taitle -->

						</div>	<!-- #category/date/taitle -->
					</section>	<!-- #loop section -->
          <?php endwhile; endif; ?>
        </div>
        <?php wp_reset_postdata(); ?>

			</div>
		</div>
	</main><!-- #main -->

	<!-- backbutton -->
	<div class="container my-5">
		<div class="row">
			<div class="col mx-auto mb-5 text-center">
				<a href="<?php echo home_url( '/' );?>">
					<button type="button" class="my-backbutton">戻る</button>
				</a>
			</div>
		</div>
	</div><!-- #backbutton -->
	
<?php
get_footer();

