<?php
/**
 * daidoko functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package daidoko
 */

if ( ! function_exists( 'daidoko_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function daidoko_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on daidoko, use a find and replace
		 * to change 'daidoko' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'daidoko', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'daidoko' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'daidoko_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'daidoko_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function daidoko_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'daidoko_content_width', 640 );
}
add_action( 'after_setup_theme', 'daidoko_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function daidoko_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'daidoko' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'daidoko' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'daidoko_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function daidoko_scripts() {
	wp_enqueue_style(	//bootstrap4 をCDN経由で読み込み
		'bootstrap4-style',
		"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	);
	wp_enqueue_style( //fontawesome をCDN経由で読み込み
		'Font_Awesome',
		"https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	);
	wp_enqueue_style( //style.css の読み込み
		'daidoko-style',
		get_stylesheet_uri()
	);

	//WordPress 本体の jQuery を登録解除
	wp_deregister_script( 'jquery');  
	
  wp_enqueue_script( //jQuery を CDN から読み込む
		'jquery', 
    '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', 
    array(), 
    '3.3.1', 
    true //</body> 終了タグの前で読み込み
	);
	wp_enqueue_script(
		'daidoko-navigation',
		get_template_directory_uri() .'/js/navigation.js',
		array(),
		'20151215',
		true
	);
	wp_enqueue_script(
		'daidoko-skip-link-focus-fix',
		get_template_directory_uri() . '/js/skip-link-focus-fix.js',
		array(),
		'20151215',
		true
	);
  wp_enqueue_script( // base.js の読み込み
		'base-script',
    get_theme_file_uri( '/js/base.js' ),
    array( 'jquery' ), //依存ファイルは上記の jquery
    filemtime( get_theme_file_path( '/js/base.js' ) ), 
    true
	);
	wp_enqueue_script(
		'popper',
		'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js',
		array('jquery'),
		'',
		true
	);
	wp_enqueue_script(
		'bootstrap4',
		'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',
		array('jquery'),
		'',
		true
	);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'daidoko_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * 固定ページのスラッグがdaidoko or hanare
 * categoryがdaidoko or hanare
 * 親ページがdaidoko or hanare
 */
function is_daidoko_or_hanare($type) {
	if(!($type == 'daidoko' || $type == 'hanare')){
		return false;
	}
	$term = get_queried_object();
	$term_slug = $term->slug;
	$category = get_category_by_slug( $type );
	if(is_home() || is_front_page()  || is_category( $type ) || (is_singular() && in_category( $category->slug )) || 'news' == $term_slug){
		return $type;
	}
	global $post;
	if ($post->post_parent) {
	  $post_data = get_post($post->post_parent);
	  return $post_data->post_name == $type ? $type : false;
	}
}