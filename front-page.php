<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package daidoko
 */

get_header();
?>


	<!-- newsarchive -->
	<div id="newsarchive" class="container">
		<div class="row">
			<div class="col col-lg-10 col-xl-7 mx-auto p-xl-0">
				<h2 class="text-center mb-3 font-weight-demibold">NEWS</h2>
				<?php
					// The Query
					$args      = array(
						'post_type'      => 'post',
						'posts_per_page' => 3, // 最大表示件数の指定
						'post_status'    => 'publish', // 公開済みのページのみ取得
					);
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
						echo '<ul class="m-0 p-0">';
						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							echo '<a href="' . get_the_permalink() . '">' . '<li class="mb-3 flex-column-reverse flex-sm-row"><div class="mr-3">' . get_the_title() . '</div><time class="font-weight-demibold ">(' . get_the_date() . ')</time></li></a>';
						}
						echo '</ul>';
						/* Restore original Post Data */
						wp_reset_postdata();
					} else {
						// no posts found
					}
					?>
				<div class="text-right mt-3 fon-weight-demibold my-readmore ">
					<?php
						$category_id   = get_cat_ID( 'News' );
						$category_link = get_category_link( $category_id );
					?>
						
					<a href="<?php echo esc_url( $category_link ); ?>" class="text-white">Read More <i class="fas fa-chevron-right"></i></a>
				</div>
				<hr class="bg-white">
			</div>
		</div>
	</div><!-- #newsarchive -->

	<!-- main -->
	<main id="main" class="site-main container mt-5">
		<div class="row">
			<div class="col col-lg-10 col-xl-9 mx-auto p-xl-0 ">
				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
				<hr class="bg-white">
			</div>
		</div>
	</main><!-- #main -->
	
	<!-- 注意喚起文言 -->
	<div class="container my-5">
		<div class="row">
			<div class="col col-lg-10 col-xl-9 mx-auto mt-4 mb-4">
				<?php echo get_page_by_path( 'attention' )->post_content;?>
			</div>
		</div>
		<div class="row">
			<div class="col col-lg-10 col-xl-9 mx-auto p-xl-0">
				<hr class="bg-white">
			</div>
		</div>
	</div><!-- #instagramプラグイン -->

	<!-- instagramプラグイン -->
	<div class="container my-5">
		<div class="row">
			<div class="col col-lg-10 col-xl-9 mx-auto mt-4 mb-4">
				<h2 class="text-center font-weight-demibold my-instagram">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/instagram.svg">
					<span>Instagram</span>
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col col-lg-10 col-xl-9 mx-auto mb-5 p-xl-0">
				<?php echo do_shortcode( '[instagram-feed num=12 cols=6]' ); ?>
			</div>
		</div>
		<div class="row">
			<div class="col col-lg-10 col-xl-9 mx-auto p-xl-0">
				<hr class="bg-white">
			</div>
		</div>
	</div><!-- #instagramプラグイン -->

	<!-- スタッフ募集中 -->
	<div class="container my-5">
		<div class="row">
			<div class="col col-lg-10 col-xl-9 mx-auto text-center">
				<a href="<?php echo get_permalink( get_page_by_path( 'requruit' ) ); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/staffbtn.svg" alt="">
				</a>
			</div>
		</div>
	</div><!-- #スタッフ募集中 -->
	
<?php
get_footer();
