<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package daidoko
 */

get_header();
?>

	<!-- title -->
	<div class="container mt-5">
		<div class="row">
			<div class="col mx-auto p-0">
				<h1 class="text-center mb-3 ">NEWS</h1>
			</div>
		</div>
	</div><!-- #title -->

	<!-- main -->
	<main id="main" class="site-main container mt-3">
		<div class="row m-0">
			<div class="col mx-auto my-bg-daidoko text-black">		
				<!-- category/date/taitle -->
				<div class="container mt-5 px-0 font-weight-demibold">
					<!-- category -->
					<div class="row">
						<div class="col-10 mx-auto my-category">
							<?php
								$category = get_the_category();
								if ($category) { 
									foreach($category as $cat){
										echo '<a href="'. get_category_link( $cat->cat_ID ). '"><img class="pr-3" src="', get_stylesheet_directory_uri() ,'/img/', $cat->category_nicename, '_logo_mini.svg" alt="', $cat->cat_name, '"></a>';
									} 
								}
							?>
						</div>
					</div>	<!-- #category -->
					<!-- date -->
					<div class="row mt-4">	
						<div class="col-10 mx-auto my-date">
							<?php echo '(' . get_the_date() . ')' //日付
							?>	
						</div>
					</div>	<!-- #date -->
					<!-- taitle -->
					<div class="row mt-3">
						<div class="col-10 mx-auto my-single-taitle">
							<?php echo the_title(); //投稿タイトル
							?>
						<hr class="bg-black mb-2">
						</div>
					</div>	<!-- #taitle -->		
				</div>	<!-- #category/date/taitle -->
					
				<!-- my-content -->
				<div class="container mb-5">
					<div class="row mx-auto">
						<div class="col mx-auto my-content px-0">
							<?php
							while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/content', 'page' );
							
							endwhile; // End of the loop.
							?>
						</div>
					</div>
				</div>	<!-- #my-content -->
			</div>
		</div>
	</main><!-- #main -->

	<!-- backbutton -->
	<div class="container my-5">
		<div class="row">
			<div class="col mx-auto mb-5 text-center">
				<a href="<?php echo home_url( '/' );?>">
					<button type="button" class="my-backbutton">戻る</button>
				</a>
			</div>
		</div>
	</div><!-- #backbutton -->

<?php
get_footer();