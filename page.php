<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package daidoko
 */

get_header();
?>

	<!-- Top画像 -->
	<div class="container">
		<div class="row">
			<div class="col mx-auto text-center">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/img/page-top.png" alt="">
			</div>
		</div>
	</div><!-- #Top画像 -->

	<!-- pagetitle -->
	<div class="container mt-5">
		<div class="row">
			<div class="col mx-auto p-0">
				<h1 class="text-center mb-3 "><?php the_title(); ?></h1>
			</div>
		</div>
	</div><!-- #pagetitle -->

	<!-- main -->
	<main id="main" class="site-main container mt-3">
		<div class="row m-0">
			<div class="col mx-auto my-bg-daidoko text-black pt-5">
				<div class="col mx-auto my-content px-0">
					<?php
						while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'page' );

						endwhile; // End of the loop.
					?>
				</div>
			</div>
		</div>
	</main><!-- #main -->

	<!-- backbutton -->
	<div class="container my-5">
		<div class="row">
			<div class="col mx-auto mb-5 text-center">
				<a href="<?php echo home_url( '/' );?>">
					<button type="button" class="my-backbutton">戻る</button>
				</a>
			</div>
		</div>
	</div><!-- #backbutton -->

<?php
get_footer();
